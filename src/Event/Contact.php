<?php
namespace App\Event;
use Cake\Event\EventListenerInterface;
use Cake\Event\Event;
use ArrayObject;

class Contact implements EventListenerInterface
{

    public function implementedEvents()
    {
        return [
            'Model.Contact.afterCreate' => 'afterCreate',
        ];
    }

    public function afterCreate(Event $event)
    {
        mail('nabinem_yakthumba@yahoo.com','new contact created ','new contact of name '.$event->data['contact']->name);
    }
}