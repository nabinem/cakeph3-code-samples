<?php
namespace App\Event;

use Cake\Event\EventListenerInterface;
use Cake\Event\Event;

class Test implements EventListenerInterface
{

    public function implementedEvents()
    {
        return [
            'Model.beforeDelete' => 'beforeDelete',
        ];
    }

    public function beforeDelete(Event $event, $query, $options)
    {
        debug(func_get_args()); die;
        // Your code here
    }
}