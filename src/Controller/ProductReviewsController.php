<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * ProductReviews Controller
 *
 * @property \App\Model\Table\ProductReviewsTable $ProductReviews
 */
class ProductReviewsController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Products', 'Users']
        ];
        $this->set('productReviews', $this->paginate($this->ProductReviews));
        $this->set('_serialize', ['productReviews']);
    }

    /**
     * View method
     *
     * @param string|null $id Product Review id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $productReview = $this->ProductReviews->get($id, [
            'contain' => ['Products', 'Users']
        ]);
        $this->set('productReview', $productReview);
        $this->set('_serialize', ['productReview']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $productReview = $this->ProductReviews->newEntity();
        if ($this->request->is('post')) {
            $productReview = $this->ProductReviews->patchEntity($productReview, $this->request->data);
            if ($this->ProductReviews->save($productReview)) {
                $this->Flash->success(__('The product review has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The product review could not be saved. Please, try again.'));
            }
        }
        $products = $this->ProductReviews->Products->find('list', ['limit' => 200]);
        $users = $this->ProductReviews->Users->find('list', ['limit' => 200]);
        $this->set(compact('productReview', 'products', 'users'));
        $this->set('_serialize', ['productReview']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Product Review id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $productReview = $this->ProductReviews->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $productReview = $this->ProductReviews->patchEntity($productReview, $this->request->data);
            if ($this->ProductReviews->save($productReview)) {
                $this->Flash->success(__('The product review has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The product review could not be saved. Please, try again.'));
            }
        }
        $products = $this->ProductReviews->Products->find('list', ['limit' => 200]);
        $users = $this->ProductReviews->Users->find('list', ['limit' => 200]);
        $this->set(compact('productReview', 'products', 'users'));
        $this->set('_serialize', ['productReview']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Product Review id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $productReview = $this->ProductReviews->get($id);
        if ($this->ProductReviews->delete($productReview)) {
            $this->Flash->success(__('The product review has been deleted.'));
        } else {
            $this->Flash->error(__('The product review could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
