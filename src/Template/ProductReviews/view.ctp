<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('Edit Product Review'), ['action' => 'edit', $productReview->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Product Review'), ['action' => 'delete', $productReview->id], ['confirm' => __('Are you sure you want to delete # {0}?', $productReview->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Product Reviews'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Product Review'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Products'), ['controller' => 'Products', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Product'), ['controller' => 'Products', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
    </ul>
</div>
<div class="productReviews view large-10 medium-9 columns">
    <h2><?= h($productReview->id) ?></h2>
    <div class="row">
        <div class="large-5 columns strings">
            <h6 class="subheader"><?= __('Product') ?></h6>
            <p><?= $productReview->has('product') ? $this->Html->link($productReview->product->name, ['controller' => 'Products', 'action' => 'view', $productReview->product->id]) : '' ?></p>
            <h6 class="subheader"><?= __('Review') ?></h6>
            <p><?= h($productReview->review) ?></p>
            <h6 class="subheader"><?= __('User') ?></h6>
            <p><?= $productReview->has('user') ? $this->Html->link($productReview->user->id, ['controller' => 'Users', 'action' => 'view', $productReview->user->id]) : '' ?></p>
            <h6 class="subheader"><?= __('Del Yn') ?></h6>
            <p><?= h($productReview->del_yn) ?></p>
        </div>
        <div class="large-2 columns numbers end">
            <h6 class="subheader"><?= __('Id') ?></h6>
            <p><?= $this->Number->format($productReview->id) ?></p>
        </div>
    </div>
</div>
