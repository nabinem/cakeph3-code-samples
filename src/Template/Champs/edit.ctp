<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $champ->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $champ->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Champs'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Contacts'), ['controller' => 'Contacts', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Contact'), ['controller' => 'Contacts', 'action' => 'add']) ?></li>
    </ul>
</div>
<div class="champs form large-10 medium-9 columns">
    <?= $this->Form->create($champ) ?>
    <fieldset>
        <legend><?= __('Edit Champ') ?></legend>
        <?php
            echo $this->Form->input('label');
            echo $this->Form->input('type');
            echo $this->Form->input('contacts._ids', ['options' => $contacts]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
