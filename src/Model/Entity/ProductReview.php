<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ProductReview Entity.
 */
class ProductReview extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'product_id' => true,
        'review' => true,
        'user_id' => true,
        'del_yn' => true,
        'product' => true,
        'user' => true,
    ];
}
