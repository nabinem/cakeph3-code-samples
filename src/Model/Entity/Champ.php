<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Champ Entity.
 */
class Champ extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'label' => true,
        'type' => true,
        'contacts' => true,
    ];
}
