<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ChampsContact Entity.
 */
class ChampsContact extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'contact_id' => true,
        'champ_id' => true,
        'value' => true,
        'contact' => true,
        'champ' => true,
    ];
}
