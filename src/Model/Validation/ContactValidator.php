<?php
// In src/Model/Validation/ContactValidator.php
namespace App\Model\Validation;

use Cake\Validation\Validator;

class ContactValidator extends Validator
{
    public function __construct()
    {
        parent::__construct();
        $validator
            ->add('validDomain','custom',[
                'rule' => function($value){
                $url = parse_url($value);
                $host = $url['host'];
                if($host != gethostbyname($host)){
                    return true;
                }
                return false;
            }
        ]);

    }
}
