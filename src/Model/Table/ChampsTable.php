<?php
namespace App\Model\Table;

use App\Model\Entity\Champ;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Champs Model
 *
 * @property \Cake\ORM\Association\BelongsToMany $Contacts
 */
class ChampsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->table('champs');
        $this->displayField('label');
        $this->primaryKey('id');
        $this->belongsToMany('Contacts', [
            'through' => 'ChampsContacts',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');
            
        $validator
            ->requirePresence('label', 'create')
            ->notEmpty('label');
            
        $validator
            ->requirePresence('type', 'create')
            ->notEmpty('type');

        return $validator;
    }
}
